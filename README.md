# Technische Anmerkungen zur Benutzung dieser Beispielanwendungen

Um die hier vorfindlichen Jupyter notebooks im Webbrowser anzusehen,
bedient man sich am besten der vorbereiteten statisch gerenderten
Ausgabe:

https://mur-at-public.gitlab.io/orchideen-ml/orchideen-klassifikation.html

Will man sie dagegen auch auf dem eigenen Linux-Rechner ausführen oder
daran weiterbasteln, geht das am vorteilhaftesten über 
[nvidia-docker](https://github.com/NVIDIA/nvidia-docker) und ein
[fertig verfügbares docker container
image](https://github.com/Paperspace/fastai-docker), das bereits alle
benötigten Python-Erweiterungen fertig installiert enthält. Damit
reduziert sich der technische Aufwand zur Reproduktion meiner Resultate
auf folgende Befehle:

1. Klonen des Beispiel-Repositories und der Lern-/Testdaten:

        mkdir orchideen-projekt
        cd orchideen-projekt
        git clone https://gitlab.com/mur-at-public/orchideen.git
        git clone https://gitlab.com/mur-at-public/orchideen-ml.git


   Es sollte nun in etwa folgenede Dateistruktur vorliegen:

        orchideen-projekt/    <== sie befinden sich "hier" !:)
	    |-- orchideen
	    |   |-- arten-512
	    |   |-- min50_training.csv
	    |   |--  min50_validation.csv
	    |  ...
	    |-- orchideen-ml
		    |-- data
		    |   |-- orchideen -> ../../../orchideen
		    |-- fastai -> ../../fastai/fastai/
		    |-- orchideen-klassifikation.ipynb
		    |-- README.ml


2. Starten des Docker Containers:

   Um vorhandene Nvidia-Grafikkarten für die Beschleunigung der
   Berechnungen nutzen zu können:

        docker run --rm --runtime=nvidia -ti -p 8888:8888 --net=host \
        -v $(pwd):/notebooks/orchideen-projekt paperspace/fastai:cuda9_pytorch0.3.1

   Zur Not dürfte es aber auch ohne GPU-Unterstützung mit gewöhnlichem
   docker funktionieren:

        docker run -rm -ti -p 8888:8888 --net=host \
        -v $(pwd):/notebooks/orchideen-projekt paperspace/fastai:cuda9_pytorch0.3.1

3. Im Terminal erscheint nach dem Start ein URL mit jeweils neu
   generiertem Schlüssel-Token, über den man Zugriff auf den Jupyter
   notebook Server erhält.

   Das Klassifikationsbeispiel findet sich in der notebook-Hierarchie unter: 
   
        /orchideen-projekt/orchideen-ml/orchideen-classification.ipynb

Mit den Nvidia-Grafikkarten bzw. der diesbezüglichen
Speicherverwaltung gibt es leider immer wieder Probleme. Wenn die
Ausführung einer Zelle mit einer Fehlermeldung beendet wird, die
irgendwas im Sinne von "cuda runtime error ... out of memory" enthält,
bleibt einem nichts anders über, als über das Menu oder den
Recycle-Button den Jupiter Kernel neu zu starten und die notwendigen
Zellen erneut zu berechnen. Generell lässt sich aber die Auslastung
und der Speicherverbrauch auf der GPU mit dem Befehl `nvidia-smi`
überwachen.
